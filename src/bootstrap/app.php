<?php

use App\Repositories\TodoRepositoryInterface;
use App\Repositories\TodosDatabaseRepository;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Support\Facades\DB;

require_once __DIR__ . '/../vendor/autoload.php';

(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(dirname(__DIR__)))->bootstrap();

date_default_timezone_set(env('APP_TIMEZONE', 'UTC'));

$app = new Laravel\Lumen\Application(dirname(__DIR__));

$app->withFacades();

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(Illuminate\Contracts\Console\Kernel::class, App\Console\Kernel::class);

$app->bind(ConnectionInterface::class, fn() => DB::connection());
$app->bind(TodoRepositoryInterface::class, TodosDatabaseRepository::class);

$app->configure('app');

$app->router->group(
    ['namespace' => 'App\Http\Controllers'],
    function ($router) {
        require __DIR__ . '/../routes/web.php';
    }
);

return $app;
