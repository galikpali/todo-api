<?php

use App\Models\TodoModel;

class TodoModelTest extends TestCase
{
    public function testToJson(): void
    {
        $model = new TodoModel();
        $model->id = 1;
        $model->isChecked = true;
        $model->description = 'Buy some milk';

        $this->assertEquals(
            '{"id":1,"isChecked":true,"description":"Buy some milk"}',
            $model->toJson()
        );
    }
}
