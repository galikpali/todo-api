<?php

use App\Models\TodoModel;
use App\Repositories\TodosDatabaseRepository;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Builder;

class TodosDatabaseRepositoryTest extends TestCase
{
    private TodosDatabaseRepository $repository;

    private Builder $builderMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->builderMock = $this->createMock(Builder::class);

        $connectionMock = $this->createMock(ConnectionInterface::class);
        $connectionMock
            ->method('table')
            ->willReturn($this->builderMock);

        $this->repository = new TodosDatabaseRepository($connectionMock);
    }

    /**
     * @param bool $queryBuilderInsertReturn
     * @param bool $expected
     * @dataProvider dataProviderTestSave
     */
    public function testSave(bool $queryBuilderInsertReturn, bool $expected): void
    {
        $model = new TodoModel();
        $model->description = 'Buy some milk';
        $model->isChecked = true;

        $this->builderMock
            ->method('insert')
            ->with([
                'is_checked' => 1,
                'description' => 'Buy some milk',
            ])
            ->willReturn($queryBuilderInsertReturn);

        $this->assertEquals($expected, $this->repository->save($model));
    }

    public function dataProviderTestSave(): array
    {
        return [
            "query builder's insert returns true, then save returns true" => [
                true,
                true,
            ],
            "query builder's insert returns false, then save returns false" => [
                false,
                false,
            ],
        ];
    }

    /**
     * @param int $affectedRows
     * @param bool $expected
     * @dataProvider dataProviderTestDeleteById
     */
    public function testDeleteById(int $affectedRows, bool $expected): void
    {
        $this->builderMock
            ->method('delete')
            ->with(9001)
            ->willReturn($affectedRows);

        $this->assertEquals($expected, $this->repository->deleteById(9001));
    }

    public function dataProviderTestDeleteById(): array
    {
        return [
            "query builder's delete affected rows returns 1, then deleteById returns true" => [
                1,
                true,
            ],
            "query builder's delete affected rows returns 0, then deleteById returns false" => [
                0,
                false,
            ],
        ];
    }

    /**
     * @param int $affectedRows
     * @param bool $expected
     * @dataProvider dataProviderTestUpdateStatusById
     */
    public function testUpdateStatusById(int $affectedRows, bool $expected): void
    {
        $this->builderMock
            ->method('where')
            ->with('id', '=', 9001)
            ->willReturnSelf();

        $this->builderMock
            ->method('update')
            ->with(['is_checked' => 1])
            ->willReturn($affectedRows);

        $this->assertEquals($expected, $this->repository->updateStatusById(9001, 1));
    }

    public function dataProviderTestUpdateStatusById(): array
    {
        return [
            "query builder's update affected rows returns 1, then deleteById returns true" => [
                1,
                true,
            ],
            "query builder's update affected rows returns 0, then deleteById returns false" => [
                0,
                false,
            ],
        ];
    }

    public function testFindById(): void
    {
        $row = $this->createRowObject(1, 1, 'Buy some milk');

        $this->builderMock
            ->method('select')
            ->willReturnSelf();
        $this->builderMock
            ->method('where')
            ->with(['id' => 1])
            ->willReturnSelf();
        $this->builderMock
            ->method('first')
            ->willReturn($row);

        $model = $this->repository->findById(1);

        $this->assertInstanceOf(TodoModel::class, $model);
        $this->assertEquals(1, $model->id);
        $this->assertEquals(true, $model->isChecked);
        $this->assertEquals('Buy some milk', $model->description);
    }

    public function testFindByIdReturnNullIfNotFound(): void
    {
        $this->builderMock
            ->method('select')
            ->willReturnSelf();
        $this->builderMock
            ->method('where')
            ->with(['id' => 1])
            ->willReturnSelf();
        $this->builderMock
            ->method('first')
            ->willReturn(null);

        $this->assertNull($this->repository->findById(1));
    }

    public function testFindAll(): void
    {
        $row = $this->createRowObject(1, 1, 'Buy some milk');

        $this->builderMock
            ->method('select')
            ->willReturnSelf();
        $this->builderMock
            ->method('get')
            ->willReturn([$row]);

        $models = $this->repository->findAll();

        $this->assertInstanceOf(TodoModel::class, $models[0]);
        $this->assertEquals(1, $models[0]->id);
        $this->assertEquals(true, $models[0]->isChecked);
        $this->assertEquals('Buy some milk', $models[0]->description);
    }

    private function createRowObject(int $id, int $isChecked, string $description): stdClass
    {
        $row = new stdClass();
        $row->id = $id;
        $row->is_checked = $isChecked;
        $row->description = $description;

        return $row;
    }
}
