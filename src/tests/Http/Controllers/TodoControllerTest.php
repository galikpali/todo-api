<?php

use App\Http\Controllers\TodoController;
use App\Models\TodoModel;
use App\Repositories\TodoRepositoryInterface;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator as ValidatorFacade;
use Illuminate\Validation\ValidationException;
use PHPUnit\Framework\MockObject\MockObject;

class TodoControllerTest extends TestCase
{
    private TodoController $controller;

    private TodoRepositoryInterface $repositoryMock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repositoryMock = $this->createMock(TodoRepositoryInterface::class);
        $this->controller = new TodoController($this->repositoryMock);
    }

    /**
     * @param bool $repositorySaveOk
     * @param int $expectedStatusCode
     * @throws Throwable
     * @throws ValidationException
     * @dataProvider dataProviderTestCreate
     */
    public function testCreate(bool $repositorySaveOk, int $expectedStatusCode): void
    {
        $validationMock = $this->createMock(Validator::class);
        $validationMock
            ->method('fails')
            ->willReturn(false);

        ValidatorFacade::shouldReceive('make')
            ->once()
            ->withAnyArgs()
            ->andReturn($validationMock);

        $requestMock = $this->createRequestMock();
        $requestMock
            ->expects($this->once())
            ->method('get')
            ->with('description')
            ->willReturn('Buy some milk');

        $requestMock
            ->expects($this->once())
            ->method('all')
            ->willReturn([]);

        $this->repositoryMock
            ->method('save')
            ->with($this->callback(function (TodoModel $model): bool {
                return $model->description === 'Buy some milk' && $model->isChecked === false;
            }))
            ->willReturn($repositorySaveOk);

        $response = $this->controller->create($requestMock);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());
    }

    public function dataProviderTestCreate(): array
    {
        return [
            'repository save returns true, then response status is 201 (Created)' => [
                true,
                Response::HTTP_CREATED,
            ],
            'repository save returns false, then response status is 500 (Internal Server Error)' => [
                false,
                Response::HTTP_INTERNAL_SERVER_ERROR,
            ],
        ];
    }

    public function testCreateWhenValidationExceptionIsThrown(): void
    {
        $validationMock = $this->createMock(Validator::class);
        $validationMock
            ->method('fails')
            ->willReturn(true);

        $requestMock = $this->createRequestMock();

        $this->expectException(ValidationException::class);

        $this->controller->create($requestMock);
    }

    public function testGetReturnsModelsJsonRepresentationIfFound(): void
    {
        $modelToReturn = $this->createMock(TodoModel::class);
        $modelToReturn
            ->method('toJson')
            ->willReturn('foo');

        $this->repositoryMock
            ->method('findById')
            ->with(1)
            ->willReturn($modelToReturn);

        $response = $this->controller->get(1);

        $this->assertEquals('foo', $response->getContent());
    }

    public function testGetReturns404IfResourceNotFound(): void
    {
        $this->repositoryMock
            ->method('findById')
            ->with(1)
            ->willReturn(null);

        $response = $this->controller->get(1);

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * @param bool $repositoryDeleteSuccess
     * @param int $expectedStatusCode
     * @dataProvider dataProviderTestDelete
     */
    public function testDelete(bool $repositoryDeleteSuccess, int $expectedStatusCode): void
    {
        $this->repositoryMock
            ->method('deleteById')
            ->with(1)
            ->willReturn($repositoryDeleteSuccess);

        $response = $this->controller->delete(1);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());
    }

    public function dataProviderTestDelete(): array
    {
        return [
            'repository delete by id returns true, then status code is 204 (No Content)' => [
                true,
                Response::HTTP_NO_CONTENT,
            ],
            'repository delete by id returns false, then status code is 404 (Not Found)' => [
                false,
                Response::HTTP_NOT_FOUND,
            ],
        ];
    }

    /**
     * @param bool $repositoryUpdateSuccess
     * @param int $expectedStatusCode
     * @dataProvider dataProviderTestCheckTestUncheck
     */
    public function testCheck(bool $repositoryUpdateSuccess, int $expectedStatusCode): void
    {
        $this->repositoryMock
            ->method('updateStatusById')
            ->willReturn($repositoryUpdateSuccess);

        $response = $this->controller->check(1);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());
    }

    /**
     * @param bool $repositoryUpdateSuccess
     * @param int $expectedStatusCode
     * @dataProvider dataProviderTestCheckTestUncheck
     */
    public function testUncheck(bool $repositoryUpdateSuccess, int $expectedStatusCode): void
    {
        $this->repositoryMock
            ->method('updateStatusById')
            ->with(1)
            ->willReturn($repositoryUpdateSuccess);

        $response = $this->controller->uncheck(1);

        $this->assertEquals($expectedStatusCode, $response->getStatusCode());
    }

    public function dataProviderTestCheckTestUncheck(): array
    {
        return [
            'repository update status by id returns true, then status code is 204 (No Content)' => [
                true,
                Response::HTTP_NO_CONTENT,
            ],
            'repository update status by id returns false, then status code is 404 (Not Found)' => [
                false,
                Response::HTTP_NOT_FOUND,
            ],
        ];
    }

    private function createRequestMock(): MockObject
    {
        $requestMock = $this->createMock(Request::class);

        $requestMock
            ->expects($this->once())
            ->method('all')
            ->willReturn([]);

        return $requestMock;
    }
}
