<?php

namespace App\Repositories;

use App\Models\TodoModel;

interface TodoRepositoryInterface
{
    public function save(TodoModel $todo): bool;

    public function findById(int $id): ?TodoModel;

    public function deleteById(int $id): bool;

    public function updateStatusById(int $id, int $status): bool;

    /** @return TodoModel[] */
    public function findAll(): array;
}
