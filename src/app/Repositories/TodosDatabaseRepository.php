<?php

namespace App\Repositories;

use App\Models\TodoModel;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Builder;
use stdClass;

class TodosDatabaseRepository implements TodoRepositoryInterface
{
    private const TABLE_NAME = 'todos';

    private ConnectionInterface $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function save(TodoModel $todo): bool
    {
        return $this->getQueryBuilder()
            ->insert([
                'description' => $todo->description,
                'is_checked' => (int)$todo->isChecked,
            ]);
    }

    public function deleteById(int $id): bool
    {
        $affectedRows = $this->getQueryBuilder()
            ->delete($id);

        return $affectedRows !== 0;
    }

    public function updateStatusById(int $id, int $status): bool
    {
        $affectedRows = $this->getQueryBuilder()
            ->where('id', '=', $id)
            ->update(['is_checked' => $status]);

        return $affectedRows !== 0;
    }

    public function findById(int $id): ?TodoModel
    {
        $row = $this->getQueryBuilder()
            ->select()
            ->where(['id' => $id])
            ->first();

        if (!$row) {
            return null;
        }

        return $this->createModelByRowObject($row);
    }

    /** @return TodoModel[] */
    public function findAll(): array
    {
        $todos = [];

        foreach ($this->getQueryBuilder()->select()->get() as $row) {
            $todos[] = $this->createModelByRowObject($row);
        }

        return $todos;
    }

    private function createModelByRowObject(stdClass $row): TodoModel
    {
        $model = new TodoModel();
        $model->id = $row->id;
        $model->isChecked = (bool)$row->is_checked;
        $model->description = $row->description;

        return $model;
    }

    private function getQueryBuilder(): Builder
    {
        return $this->connection->table(self::TABLE_NAME);
    }
}
