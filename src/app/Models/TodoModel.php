<?php

namespace App\Models;

use Illuminate\Contracts\Support\Jsonable;

class TodoModel implements Jsonable
{
    public int $id;

    public bool $isChecked = false;

    public string $description;

    public function toJson($options = 0): string
    {
        return json_encode([
            'id' => $this->id,
            'isChecked' => $this->isChecked,
            'description' => $this->description,
        ]);
    }
}
