<?php

namespace App\Http\Controllers;

use App\Models\TodoModel;
use App\Repositories\TodoRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;
use Throwable;

class TodoController extends Controller
{
    private TodoRepositoryInterface $repository;

    public function __construct(TodoRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Throwable
     * @throws ValidationException
     */
    public function create(Request $request): Response
    {
        $this->validate($request, ['description' => 'string|required']);

        $model = new TodoModel();
        $model->isChecked = false;
        $model->description = $request->get('description');

        $statusCode = $this->repository->save($model)
            ? Response::HTTP_CREATED
            : Response::HTTP_INTERNAL_SERVER_ERROR;

        return (new Response())->setStatusCode($statusCode);
    }

    public function get(int $id): Response
    {
        $model = $this->repository->findById($id);

        if (!$model) {
            return (new Response())->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        return (new Response())->setContent($model);
    }

    public function delete(int $id): Response
    {
        $statusCode = $this->repository->deleteById($id)
            ? Response::HTTP_NO_CONTENT
            : Response::HTTP_NOT_FOUND;

        return (new Response())->setStatusCode($statusCode);
    }

    public function check(int $id): Response
    {
        $statusCode = $this->repository->updateStatusById($id, 1)
            ? Response::HTTP_NO_CONTENT
            : Response::HTTP_NOT_FOUND;

        return (new Response())->setStatusCode($statusCode);
    }

    public function uncheck(int $id): Response
    {
        $statusCode = $this->repository->updateStatusById($id, 0)
            ? Response::HTTP_NO_CONTENT
            : Response::HTTP_NOT_FOUND;

        return (new Response())->setStatusCode($statusCode);
    }

    public function listAll(): Response
    {
        return (new Response())->setContent($this->repository->findAll());
    }
}
