<?php

use Laravel\Lumen\Routing\Router;

/** @var Router $router */
$router->post('/todo', 'TodoController@create');
$router->get('/todo/{id:\d*}', 'TodoController@get');
$router->delete('/todo/{id:\d*}', 'TodoController@delete');
$router->patch('/todo/{id:\d*}/check', 'TodoController@check');
$router->patch('/todo/{id:\d*}/uncheck', 'TodoController@uncheck');
$router->get('/todos', 'TodoController@listAll');
