<?php

use App\Models\TodoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodosTable extends Migration
{
    public function up(): void
    {
        Schema::create('todos', function (Blueprint $table): void {
            $table->bigIncrements('id');
            $table->tinyInteger('is_checked')->default(0);
            $table->char('description');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('todos');
    }
}
