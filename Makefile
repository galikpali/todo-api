docker_build:
	@docker build \
		-t todolist.dev \
		-f build/Dockerfile .

dev:
	@docker-compose down && \
		docker-compose build && \
		docker-compose \
			-f docker-compose.yml \
			-f docker-compose.dev.yml \
		up -d --remove-orphans

test_unit: dev
	docker exec -it todo-app vendor/bin/phpunit
	docker-compose down