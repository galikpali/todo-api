# TODO API

## REST API documentation
The API documentation can be found [here](./api/openapi.yml)

## How to run tests
1. Make sure you have a running docker daemon on your machine
2. The docker engine must be version 18.06 or higher in order to launch the project
3. Build the image by running the following command
    ```
        make docker_build
    ```

4. Run the tests by running the following command
    ```
        make test_unit
    ```